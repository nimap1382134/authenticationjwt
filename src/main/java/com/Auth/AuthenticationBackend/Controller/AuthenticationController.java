package com.Auth.AuthenticationBackend.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Auth.AuthenticationBackend.Models.UserRole;
import com.Auth.AuthenticationBackend.dto.AuthenticationResponse;
import com.Auth.AuthenticationBackend.dto.loginDto;
import com.Auth.AuthenticationBackend.dto.userDto;
import com.Auth.AuthenticationBackend.services.AuthenticationService;
import com.Auth.AuthenticationBackend.services.userService;

@RestController
@RequestMapping("/api")
public class AuthenticationController {

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private userService userser;

	@PostMapping("/register")
	public ResponseEntity<AuthenticationResponse> register(@RequestBody userDto userdto) {
		return ResponseEntity.ok(authenticationService.register(userdto));
	}

	@PostMapping("/login")
	public ResponseEntity<AuthenticationResponse> register(@RequestBody loginDto logindto) {
		return ResponseEntity.ok(authenticationService.login(logindto));
	}

	@PostMapping("/adduser-role")
	public ResponseEntity<String> adddata(@RequestBody UserRole userrole) {

		userser.create(userrole);
		return ResponseEntity.ok("data will be added");
	}

}
