package com.Auth.AuthenticationBackend.Configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.Auth.AuthenticationBackend.repository.UserReposistory;

@Configuration
public class AplicationConfig {

	@Autowired
	private UserReposistory userrepo;

	@Bean
	UserDetailsService userDetailsService() {
		return username -> userrepo.findByEmail(username).orElseThrow(() -> new RuntimeException("Email not found!"));
	}

	@Bean
	AuthenticationManager authManager(AuthenticationConfiguration configuration) throws Exception {
		return configuration.getAuthenticationManager();
	}

	@Bean
	AuthenticationProvider authprovider() {

		DaoAuthenticationProvider daoProvider = new DaoAuthenticationProvider();
		daoProvider.setUserDetailsService(userDetailsService());
		daoProvider.setPasswordEncoder(passwordEncode());
		return daoProvider;
	}

	@Bean
	PasswordEncoder passwordEncode() {
		// TODO Auto-generated method stub
		return new BCryptPasswordEncoder();
	}
}
