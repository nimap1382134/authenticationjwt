package com.Auth.AuthenticationBackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Auth.AuthenticationBackend.Models.UserRole;

public interface userroleRepo extends JpaRepository<UserRole, Integer> {

}
