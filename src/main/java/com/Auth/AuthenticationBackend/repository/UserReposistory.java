package com.Auth.AuthenticationBackend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Auth.AuthenticationBackend.Models.user;

public interface UserReposistory extends JpaRepository<user, Integer> {

	Optional<user> findByEmail(String email);

}
