package com.Auth.AuthenticationBackend.services;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Service
public class JwtService {
	private static final String SECRET_KEY = "e1d2a5d7a2170a808e6c4c3c3501d2f69c2b3b718b816f05b9ac7d3d883c23fc";

	public String extractusername(String token) {
		return extractClaim(token, Claims::getSubject);
	}

	public <T> T extractClaim(String token, Function<Claims, T> claimResolver) {
		final Claims claim = getAllClaims(token);

		return claimResolver.apply(claim);
	}

	public Claims getAllClaims(String token) {
		return Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(token).getBody();

	}

	private Key getSignInKey() {
		byte keybyte[] = Decoders.BASE64.decode(SECRET_KEY);
		return Keys.hmacShaKeyFor(keybyte);
	}

	public String generateToken(Map<String, Object> extraclaims, UserDetails userDetails) {
		return Jwts.builder().setClaims(extraclaims).setSubject(userDetails.getUsername())
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 24))
				.signWith(getSignInKey(), SignatureAlgorithm.HS256).compact();
	}

	public String generateToken(UserDetails userDetails) {
		return generateToken(new HashMap<>(), userDetails);

	}

	public boolean isValidToken(String token, UserDetails userDetails) {
		final String username = extractusername(token);
		return username.equals(userDetails.getUsername()) && !isExpiredtoken(token);

	}

	private boolean isExpiredtoken(String token) {

		return extractExpiration(token).before(new Date());
	}

	private Date extractExpiration(String token) {

		return extractClaim(token, Claims::getExpiration);
	}

}
