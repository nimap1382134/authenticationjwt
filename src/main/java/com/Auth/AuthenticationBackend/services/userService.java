package com.Auth.AuthenticationBackend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Auth.AuthenticationBackend.Models.UserRole;
import com.Auth.AuthenticationBackend.repository.userroleRepo;

@Service
public class userService {

	@Autowired
	private userroleRepo usero;

	public UserRole create(UserRole userRole) {
		return usero.save(userRole);
	}

}
