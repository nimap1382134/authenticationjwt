package com.Auth.AuthenticationBackend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.Auth.AuthenticationBackend.Models.user;
import com.Auth.AuthenticationBackend.dto.AuthenticationResponse;
import com.Auth.AuthenticationBackend.dto.loginDto;
import com.Auth.AuthenticationBackend.dto.userDto;
import com.Auth.AuthenticationBackend.repository.UserReposistory;

@Service
public class AuthenticationService {

	@Autowired
	private UserReposistory useRepo;

	@Autowired
	private PasswordEncoder passencode;

	@Autowired
	private JwtService jwt;

	@Autowired
	private AuthenticationManager authmanager;

	public AuthenticationResponse register(userDto userdto) {

		var users = user.builder().username(userdto.getUsername()).email(userdto.getEmail())
				.password(passencode.encode(userdto.getPassword())).build();

		useRepo.save(users);

		var jwttoken = jwt.generateToken(users);

		return AuthenticationResponse.builder().token(jwttoken).build();

	}

	public AuthenticationResponse login(loginDto logindto) {

		authmanager.authenticate(new UsernamePasswordAuthenticationToken(logindto.getEmail(), logindto.getPassword()));

		var users = useRepo.findByEmail(logindto.getEmail()).orElseThrow();

		var jwttoken = jwt.generateToken(users);

		return AuthenticationResponse.builder().token(jwttoken).build();
	}

}
