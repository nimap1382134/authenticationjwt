package com.Auth.AuthenticationBackend.Models;

import org.springframework.security.core.GrantedAuthority;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "user_role")
public class UserRole implements GrantedAuthority {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_role_id")
	private Integer id;

	@ManyToOne
	private user us;

	@ManyToOne
	private Role ro;

	private String authorities;

	public UserRole() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserRole(Integer id, user users, Role roles, String authorities) {
		super();
		this.id = id;
		this.us = users;
		this.ro = roles;
		this.authorities = authorities;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public user getUsers() {
		return us;
	}

	public void setUsers(user users) {
		this.us = users;
	}

	public Role getRoles() {
		return ro;
	}

	public void setRoles(Role roles) {
		this.ro = roles;
	}

	public String getAuthorities() {
		return authorities;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return authorities;
	}

}
